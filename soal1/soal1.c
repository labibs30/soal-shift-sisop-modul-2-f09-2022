#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <wait.h>
#include <json-c/json.h>

int gacha = 0;
int jumlahGacha;
int primogems = 79000;

void makeFolder(char *path)
{
    int status=0;
    if(fork()==0)
    {
        char buf1[256];
        snprintf(buf1, sizeof buf1, "%s", path);
        char *argv[] = {"mkdir", "-p", buf1, NULL};
        execv("/bin/mkdir", argv);
    }
    while(wait(&status)>0);
}

void removeZip()
{
    pid_t procTunggu, pid1 = fork(), pid2 = fork();
    if(pid1 > 0 && pid2 > 0)
    {
        procTunggu = wait(NULL);
        procTunggu = wait(NULL);
    }
    else if(pid1 == 0 && pid2 > 0)
    {
        char *args[] = {"/bin/rm", "rm", "-f", "char.zip", NULL};
        if(execv(args[0],args+1) == -1) perror("Gagal menghapus char.zip"), exit(EXIT_FAILURE);
    }
    else if(pid1 > 0 && pid2 == 0)
    {
        char *args[] = {"/bin/rm", "rm", "-f", "weapon.zip", NULL};
        if(execv(args[0],args+1) == -1) perror("Gagal menghapus weapon.zip"), exit(EXIT_FAILURE);
    }
    else if(pid1 < 0 && pid2 < 0) perror("Child removing (rm) error."), exit(EXIT_FAILURE);
}

char* gachaFileName(char *base, int gachaNum, int i)
{
    char path[1000];
    struct dirent *dt;
    DIR *dir = opendir(base);
    char *result;
    int found = 0;
    while ((dt = readdir(dir)) != NULL && found == 0)
    {
        if (strcmp(dt->d_name, ".") != 0 && strcmp(dt->d_name, "..") != 0)
        {
            result = dt->d_name;
            if (i == gachaNum) found = 1;
            i++;
        }
    }
    closedir(dir);
    return result;
}

char* runGacha(char *base, int gachaNum)
{
    id_t child_id;
    int status=0;
    child_id = fork();
    if (child_id < 0) exit(EXIT_FAILURE);
    if (child_id > 0)
    {
        while(wait(&status) > 0);
        return gachaFileName(base, gachaNum, 0);
    }
}

void unzipDB()
{
    pid_t procTunggu, pid1 = fork(), pid2 = fork();
    if(pid1 > 0 && pid2 > 0)
    {
        procTunggu = printf("Mengunzip: %d %d\n", pid1, pid2);
        wait(NULL);
        procTunggu = wait(NULL);
        removeZip();
    }
    else if(pid1 == 0 && pid2 > 0)
    {
        char *args[] = {"/bin/unzip", "unzip", "-qq", "-n", "char.zip", NULL};
        if(execv(args[0],args+1) == -1) perror("Gagal mengunzil char.zip"), exit(EXIT_FAILURE);
    }
    else if(pid1 > 0 && pid2 == 0)
    {
        char *args[] = {"/bin/unzip", "unzip", "-qq", "-n", "weapon.zip", NULL};
        if(execv(args[0],args+1) == -1) perror("Gagal mengunzip weapon.zip"), exit(EXIT_FAILURE);
    }
    else if(pid1 < 0 && pid2 < 0) perror("Child buat unzip error."), exit(EXIT_FAILURE);
}

void setupDB()
{
    pid_t procTunggu, pid1 = fork(), pid2 = fork();
    char db_char[] = "https://drive.google.com/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download";
    char db_weapon[] = "https://drive.google.com/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download";
    char charzip[] = "char.zip", weaponzip[] = "weapon.zip";
    if(pid1 > 0 && pid2 > 0)
    {
        procTunggu = wait(NULL);
        procTunggu = wait(NULL);
        unzipDB();
    }
    else if(pid1 == 0 && pid2 > 0)
    {
        char *args[] = {"/bin/wget", "wget", "--quiet" , "--no-check-certificate", db_char, "-O", charzip, NULL};
        if(execv(args[0],args+1) == -1) perror("Wget buat gacha zip characters gagal."), exit(EXIT_FAILURE);
    }
    else if(pid1 > 0 && pid2 == 0)
    {
        char *args[] = {"/bin/wget", "wget", "--quiet" , "--no-check-certificate", db_weapon, "-O", weaponzip, NULL};
        if(execv(args[0],args+1) == -1) perror("Wget buat gacha zip weapons gagal."), exit(EXIT_FAILURE);
    }
    else if(pid1 < 0 && pid2 < 0) perror("Child buat wget gagal."), exit(EXIT_FAILURE);
}

int main()
{
    pid_t pid = getpid();
    setupDB();
    if(pid != getpid()) exit(EXIT_SUCCESS);
    makeFolder("gacha_gacha");
    if(pid != getpid()) exit(EXIT_SUCCESS);
    int zip = 0, done = 0;
    while(1)
    {
        time_t rawtime = time(NULL);
        struct tm timeinfo = *localtime(&rawtime);
        if(timeinfo.tm_mday == 30 && timeinfo.tm_mon + 1 == 3 && timeinfo.tm_hour == 4 && timeinfo.tm_min == 44 && gacha <= 600 && primogems >= 160)
        // if(timeinfo.tm_mday == 27 && timeinfo.tm_mon + 1 == 3 && timeinfo.tm_hour == 15 && timeinfo.tm_min == 50  && gacha <= 600 && primogems >= 160)
        {
            // Jam 4:44 anniv
            char currFolder[200];
            while(primogems >= 160)
            {
                if(gacha%90==0)
                {
                    // folder baru --> file .txt selanjutnya di sini
                    // total_gacha_{jumlah-gacha}: total_gacha_270
                    memset(currFolder, '\0', 200*(sizeof(currFolder[0])));
                    strcpy(currFolder, "gacha_gacha/");
                    char folderName[100] = "total_gacha_";
                    char numGacha[100];
                    sprintf(numGacha, "%d", gacha);
                    strcat(folderName, numGacha);
                    strcat(currFolder, folderName);
                    makeFolder(currFolder);
                    gacha++;
                }
                else if(gacha%10==0)
                {
                    int x = 0;
                    for(int i = gacha; i < gacha + 90; i++)
                    {
                        if(primogems < 160) break;
                        printf("primo: %d\n", primogems);
                        time_t current_time;
                        struct tm * time_info;
                        char filename[100];
                        time(&current_time);
                        time_info = localtime(&current_time);
                        strftime(filename, sizeof(filename), "%H:%M:%S", time_info);
                        strcat(filename, "_gacha_");
                        char numGacha[100];
                        sprintf(numGacha, "%d", i-x-10);
                        strcat(filename, numGacha); strcat(filename, ".txt");
                        // format hasil: {jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}: 157_characters_5_Albedo_53880
                        // setor data ke file .txt (data_prev.txt -> file .txt sementara)
                        for(int j=0; j<10;j++)
                        {
                            if (primogems >= 160 && !done)
                            {
                                primogems-=160; //penamaan pada list gacha dlm txt: 1_characters
                                char buf1[10000], counter[100];
                                sprintf(counter, "%d", i-x-9);
                                char pathFolder[100] = "gacha_gacha", outputFile[200] = "";
                                strcat(outputFile, counter);
                                strcat(outputFile, "_");
                                char tipe[15];
                                int mode; // mode 1: weapons, mode 2: character
                                if((i-x-9)%2==0) mode = 1;
                                else mode = 2;
                                if(mode == 1) strcpy(tipe, "weapons_");
                                else strcpy(tipe, "characters_");
                                strcat(outputFile, tipe);
                                FILE *fp; //penamaan pada list gacha dlm txt: _4_Yanfei_78840
                                char buffer[5000];
                                struct json_object *parsed_json;
                                struct json_object *nameJson;
                                struct json_object *rarityJson;
                                char jsonPath[300]="/home/gaudhiwaa/";
                                int gachaVal;
                                if(mode == 1) strcat(jsonPath, "weapons"), gachaVal = rand()%130;
                                else strcat(jsonPath, "characters"), gachaVal = rand()%48;
                                char jsonFileName[100];
                                strcpy(jsonFileName, gachaFileName(jsonPath,gachaVal, 0));
                                strcat(jsonPath, "/");
                                strcat(jsonPath, jsonFileName);
                                fp = fopen(jsonPath,"r");
                                fread(buffer, 5000, 1, fp);
                                fclose(fp);
                                parsed_json = json_tokener_parse(buffer);
                                json_object_object_get_ex(parsed_json, "name", &nameJson);
                                json_object_object_get_ex(parsed_json, "rarity", &rarityJson);
                                char rarity[2];
                                strcpy(rarity, json_object_get_string(rarityJson));
                                printf("rarity: %s\n", rarity);
                                char name[100];
                                strcpy(name, json_object_get_string(nameJson));
                                printf("name: %s\n", name);
                                strcat(outputFile, rarity); strcat(outputFile, "_");
                                strcat(outputFile, name); strcat(outputFile, "_");
                                char primoLeft[100];
                                sprintf(primoLeft, "%d", primogems);
                                strcat(outputFile, primoLeft);
                                printf("outputFile: %s\n", outputFile);
                                char txtPath[100];
                                strcpy(txtPath, currFolder);
                                strcat(txtPath, "/");
                                strcat(txtPath, filename);
                                printf("txtpath: %s\n", txtPath);
                                snprintf(buf1, sizeof buf1, "%s", txtPath);
                                FILE *data;
                                data = fopen(buf1, "a+");
                                fprintf(data, "%s\n", outputFile);
                                fclose(data);
                                i++;
                                sleep(0.1);
                            }
                            else done = 1;
                        }
                        x++;
                        sleep(1);
                    }
                    gacha+=80;
                }
                else gacha++;
            }
            if(primogems < 160) printf("Gachanya selesai, primogems habis.");
        }
        // else if(timeinfo.tm_mday == 27 && timeinfo.tm_mon + 1 == 3 && timeinfo.tm_hour == 15 && timeinfo.tm_min == 52)
        else if(timeinfo.tm_mday == 30 && timeinfo.tm_mon + 1 == 3 && timeinfo.tm_hour == 7 && timeinfo.tm_min == 44)
        {
            // Jam 7:44 setelah anniv
            if(!zip)
            {
                zip = 1;
                while(wait(NULL) > 0);
                pid_t pid = fork();
                if (pid < 0) exit(EXIT_FAILURE);
                if (pid == 0)
                {
                    char *arg10[] = {"zip", "-r", "not_safe_for_wibu.zip", "gacha_gacha", "--password", "satuduatiga", NULL};
                    execv("/bin/zip", arg10);
                }

                while(wait(NULL) > 0);
                pid = fork();
                if (pid < 0) exit(EXIT_FAILURE);
                if (pid == 0)
                {
                    char *arg11[] = {"rm", "-r", "gacha_gacha", NULL};
                    execv("/bin/rm", arg11);
                }

                while(wait(NULL) > 0);
                pid = fork();
                if (pid < 0) exit(EXIT_FAILURE);
                if (pid == 0)
                {
                    char *arg11[] = {"rm", "-r", "characters", NULL};
                    execv("/bin/rm", arg11);
                }

                while(wait(NULL) > 0);
                pid = fork();
                if (pid < 0) exit(EXIT_FAILURE);
                if (pid == 0)
                {
                    char *arg11[] = {"rm", "-r", "weapons", NULL};
                    execv("/bin/rm", arg11);
                }
            }
            else return 0;
        }
    }
}
