#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <dirent.h>
#include <string.h>
#include <sys/stat.h>
#include <pwd.h>

void listFile(){
    FILE *fptr;
    struct dirent *dt;
    char path[] = "/home/gaudhiwaa/modul2/air/";
    DIR *dir = opendir(path);

    fptr = fopen("/home/gaudhiwaa/modul2/air/list.txt", "w");
    if(fptr == NULL){
      printf("ERROR!\n");
      exit(1);
    }

    while ((dt = readdir(dir)) != NULL){
      struct stat fs;
      int r = stat(path, &fs);
      if(r == -1){
        fprintf(stderr, "File Error\n");
        exit(1);
      }
      register struct passwd *pw;
      uid_t uid = getuid();
      pw = getpwuid(uid);
      if(strcmp(dt->d_name,".") != 0 && strcmp(dt->d_name, "..") != 0 && strcmp(dt->d_name, "list.txt") != 0){
        char userp[256] = "";
        if(fs.st_mode & S_IRUSR)
          strcat(userp, "r");
        if(fs.st_mode & S_IWUSR)
          strcat(userp, "w");
        if(fs.st_mode & S_IXUSR)
          strcat(userp, "x");
        fprintf(fptr,"%s_%s_%s\n",pw->pw_name,userp, dt->d_name);
      }
    }
    fclose(fptr);
}


int main() {
  pid_t child_id, child_id2, child_id3, child_id4, child_id5, child_id6, child_id7, child_id8, child_id9;
  int status;

  child_id = fork();
  FILE * fp;

  char *list_bird[]={"find","/home/gaudhiwaa/modul2/air","-iname","*air*.jpg", NULL};  
  
  if (child_id < 0) {
    exit(EXIT_FAILURE); 
  }
      if (child_id == 0) {
        
        char *argv[] = {"mkdir", "-p", "/home/gaudhiwaa/modul2", NULL};
        execv("/bin/mkdir", argv);
      } else {
        while ((wait(&status)) > 0);
        child_id2 = fork();

        if (child_id2 == 0) {
          char *arg[] = {"unzip","-qq","/home/gaudhiwaa/Downloads/animal.zip","-d","./", NULL};
          execv("/usr/bin/unzip",arg);
          
        } else {
          while ((wait(&status)) > 0);
          child_id3 = fork();
          
          if (child_id3 == 0) {
            char *argv[] = {"mkdir", "-p", "/home/gaudhiwaa/modul2/darat", NULL};
            execv("/bin/mkdir", argv);
          
          } else {
            while ((wait(&status)) > 0);
            sleep(3);
            child_id4 = fork();
            
            if(child_id4 == 0) {

              char *argv[] = {"mkdir", "-p", "/home/gaudhiwaa/modul2/air", NULL};
              execv("/bin/mkdir", argv);
            } else {

                while ((wait(&status)) > 0);
                child_id5 = fork();

                if(child_id5 == 0) {

                  char *darat[]={"find","/home/gaudhiwaa/Downloads/animal","-iname","*darat*.jpg","-exec","cp","{}","/home/gaudhiwaa/modul2/darat",";",NULL};  
                  execv("/usr/bin/find",darat);
                } else {

                    while ((wait(&status)) > 0);
                    child_id6 = fork();

                    if(child_id6 == 0) {

                        char *air[]={"find","/home/gaudhiwaa/Downloads/animal","-iname","*air*.jpg","-exec","cp","{}","/home/gaudhiwaa/modul2/air",";",NULL};  
                        execv("/usr/bin/find",air);
                    } else {
                        while ((wait(&status)) > 0);
                        child_id7 = fork();

                        if(child_id7 == 0) {

                          char *rm_notDaratAir[]={"rm", "-r" ,"/home/gaudhiwaa/Downloads/animal", NULL};  
                          execv("/bin/rm", rm_notDaratAir);
                        } else {
                          while ((wait(&status)) > 0);
                          child_id8 = fork();

                          if(child_id8 == 0) {

                            char *rm_bird[]={"find","/home/gaudhiwaa/modul2/darat","-iname","*bird*.jpg","-exec","rm", "-rf","{}",";", NULL};  
                            execv("/usr/bin/find", rm_bird);
                          } else {

                            while ((wait(&status)) > 0);
                            child_id9 = fork();  
                                if(child_id9 == 0) {
                                      listFile();
                                      // if(chdir("/home/gaudhiwaa/modul2/air")==0) {
                                      //     char *list_bird[]={"find",".","-iname","*air*.jpg", "-print",">", "/home/gaudhiwaa/modul2/air/list.txt", ";"};  
                                      //     execv("/usr/bin/find",list_bird);
                                      }   
                                    } 
                                } 
                            }
                      }
                  }  
              }  
           }
         }
       }
    
 
