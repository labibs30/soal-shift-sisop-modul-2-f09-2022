# soal-shift-sisop-modul-2-F09-2022

Anggota Kelompok :
* Gaudhiwaa Hendrasto	5025201066
* M Labib Alfaraby      5025201083

***
1. Mas Refadi adalah seorang wibu gemink.  Dan jelas game favoritnya adalah bengshin impek. Terlebih pada game tersebut ada sistem gacha item yang membuat orang-orang selalu ketagihan untuk terus melakukan nya. Tidak terkecuali dengan mas Refadi sendiri. Karena rasa penasaran bagaimana sistem gacha bekerja, maka dia ingin membuat sebuah program untuk men-simulasi sistem history gacha item pada game tersebut. Tetapi karena dia lebih suka nge-wibu dibanding ngoding, maka dia meminta bantuanmu untuk membuatkan program nya. Sebagai seorang programmer handal, bantulah mas Refadi untuk memenuhi keinginan nya itu. 
    
    **a**. Saat program pertama kali berjalan. Program akan mendownload file characters dan file weapons dari link yang ada **dibawah**, lalu program akan mengekstrak kedua file tersebut. File tersebut akan digunakan sebagai database untuk melakukan gacha item characters dan weapons. Kemudian akan dibuat sebuah folder dengan nama `“gacha_gacha”` sebagai working directory. Seluruh hasil gacha akan berada di dalam folder tersebut. **Penjelasan sistem gacha ada di poin (d)**.
    
    **b**. Mas Refadi ingin agar setiap kali gacha, item characters dan item weapon akan selalu bergantian diambil datanya dari database. Maka untuk setiap kali jumlah-gacha nya bernilai genap akan dilakukan gacha item weapons, jika bernilai ganjil maka item characters. Lalu untuk setiap kali jumlah-gacha nya mod 10, maka akan dibuat sebuah file baru (.txt) dan output hasil gacha selanjutnya akan berada di dalam file baru tersebut. Dan setiap kali jumlah-gacha nya mod 90, maka akan dibuat sebuah folder baru dan file (.txt) selanjutnya akan berada didalam folder baru tersebut.  **Sehingga untuk setiap folder, akan terdapat 9 file (.txt) yang didalamnya berisi 10 hasil gacha**. Dan karena ini simulasi gacha, maka hasil gacha di dalam file .txt adalah **ACAK/RANDOM** dan setiap file (.txt) isi nya akan **BERBEDA**
   
    **c**. Format penamaan setiap file (.txt) nya adalah `{Hh:Mm:Ss}_gacha_{jumlah-gacha}`, misal **04:44:12_gacha_120.txt**, dan format penamaan untuk setiap folder nya adalah `total_gacha_{jumlah-gacha}`, misal **total_gacha_27**. Dan untuk setiap file (.txt) akan memiliki perbedaan penamaan waktu output sebesar **1 second**.
    
    **d**. Pada game tersebut, untuk melakukan gacha item kita harus menggunakan alat tukar yang dinamakan primogems. Satu kali gacha item akan menghabiskan primogems sebanyak 160 primogems. Karena mas Refadi ingin agar hasil simulasi gacha nya terlihat banyak, maka pada program, primogems di awal di-define sebanyak 79000 primogems. Setiap kali gacha, ada 2 properties yang akan diambil dari database, yaitu name dan rarity. Lalu Outpukan hasil gacha nya ke dalam file (.txt) dengan format hasil gacha `{jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}`. Program akan selalu melakukan gacha hingga primogems habis.
**Contoh : 157_characters_5_Albedo_53880**
    
    **e**. Proses untuk melakukan gacha item akan dimulai bertepatan dengan anniversary pertama kali mas Refadi bermain bengshin impek, yaitu pada **30 Maret jam 04:44**.  Kemudian agar hasil gacha nya tidak dilihat oleh teman kos nya, maka 3 jam setelah anniversary tersebut semua isi di folder `gacha_gacha` akan di zip dengan nama `not_safe_for_wibu` dengan dipassword `"satuduatiga"`, lalu semua folder akan di delete sehingga hanya menyisakan file (.zip)

### Penyelesaian Soal 1
Pertama-tama, panggil fungsi setupDB dan makeFolder
```c
int main()
{
    pid_t pidUtama = getpid();
    setupDB();
    if(pidUtama != getpid()) exit(EXIT_SUCCESS);
    makeFolder("gacha_gacha");
```

Didalam fungsi setupDB program akan mendownload file weapons dan character dari link.
```c
void setupDB()
{
    pid_t procTunggu, pid1 = fork(), pid2 = fork();
    char db_char[] = "https://drive.google.com/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download";
    char db_weapon[] = "https://drive.google.com/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download";
    char charzip[] = "char.zip", weaponzip[] = "weapon.zip";
    if(pid1 > 0 && pid2 > 0)
    {
        procTunggu = wait(NULL);
        procTunggu = wait(NULL);
        unzipDB();
    }
    else if(pid1 == 0 && pid2 > 0)
    {
        char *args[] = {"/bin/wget", "wget", "--quiet" , "--no-check-certificate", db_char, "-O", charzip, NULL};
        if(execv(args[0],args+1) == -1) perror("Wget buat gacha zip characters gagal."), exit(EXIT_FAILURE);
    }
    else if(pid1 > 0 && pid2 == 0)
    {
        char *args[] = {"/bin/wget", "wget", "--quiet" , "--no-check-certificate", db_weapon, "-O", weaponzip, NULL};
        if(execv(args[0],args+1) == -1) perror("Wget buat gacha zip weapons gagal."), exit(EXIT_FAILURE);
    }
    else if(pid1 < 0 && pid2 < 0) perror("Child buat wget gagal."), exit(EXIT_FAILURE);
}
```

Jika download berhasil dilakukan maka program akan mengunzip file pada fungsi unzipDB() seperti berikut
```c
void unzipDB()
{
    pid_t procTunggu, pid1 = fork(), pid2 = fork();
    if(pid1 > 0 && pid2 > 0)
    {
        procTunggu = printf("Mengunzip: %d %d\n", pid1, pid2);
        wait(NULL);
        procTunggu = wait(NULL);
        removeZip();
    }
    else if(pid1 == 0 && pid2 > 0)
    {
        char *args[] = {"/bin/unzip", "unzip", "-qq", "-n", "char.zip", NULL};
        if(execv(args[0],args+1) == -1) perror("Gagal mengunzil char.zip"), exit(EXIT_FAILURE);
    }
    else if(pid1 > 0 && pid2 == 0)
    {
        char *args[] = {"/bin/unzip", "unzip", "-qq", "-n", "weapon.zip", NULL};
        if(execv(args[0],args+1) == -1) perror("Gagal mengunzip weapon.zip"), exit(EXIT_FAILURE);
    }
    else if(pid1 < 0 && pid2 < 0) perror("Child buat unzip error."), exit(EXIT_FAILURE);
}
```

Jika program selesai mengunzip file yang didownload, kemudian program akan menghapus file zip weapons dan character menggunakan fungsi removeZip()
```c
void removeZip()
{
    // buang zipnya
    pid_t procTunggu, pid1 = fork(), pid2 = fork();
    if(pid1 > 0 && pid2 > 0)
    {
        procTunggu = wait(NULL);
        procTunggu = wait(NULL);
    }
    else if(pid1 == 0 && pid2 > 0)
    {
        char *args[] = {"/bin/rm", "rm", "-f", "char.zip", NULL};
        if(execv(args[0],args+1) == -1) perror("Gagal menghapus char.zip"), exit(EXIT_FAILURE);
    }
    else if(pid1 > 0 && pid2 == 0)
    {
        char *args[] = {"/bin/rm", "rm", "-f", "weapon.zip", NULL};
        if(execv(args[0],args+1) == -1) perror("Gagal menghapus weapon.zip"), exit(EXIT_FAILURE);
    }
    else if(pid1 < 0 && pid2 < 0) perror("Child removing (rm) error."), exit(EXIT_FAILURE);
}
```

Kemudian menggunakan fungsi makeFolder akan dibuat sebuah folder bernama gacha_gacha menggunakan fork exec.
```c
void makeFolder(char *path)
{
    // membuat folder custom
    int status = 0;
    if(fork()==0)
    {
        char buf1[256];
        snprintf(buf1, sizeof buf1, "%s", path);
        char *argv[] = {"mkdir", "-p", buf1, NULL};
        execv("/bin/mkdir", argv);
    }
    while(wait(&status)>0);
}
```

Kemudian jika timeinfo sama dengan 30 maret jam 4.44, dengan menggunakan loop while program akan berjalan jika primogems lebih besar dari 160, jika kurang dari 160 program gacha akan berhenti, dan setiap gacha berjalan primogems akan berkurang sebanyak 160. Lalu program mengecek apakah jumlah gacha nya mod 90, jika iya maka akan dibuat sebuah folder baru dengan nama total_gacha_{jumlah-gacha}.
```c
if(gacha%90==0)
{
    memset(currFolder, '\0', 200*(sizeof(currFolder[0])));
    strcpy(currFolder, "gacha_gacha/");
    char folderName[100] = "total_gacha_";
    char numGacha[100];
    sprintf(numGacha, "%d", gacha);
    strcat(folderName, numGacha);
    strcat(currFolder, folderName);
    makeFolder(currFolder);
    gacha++;
}
```

Jika hasil gachanya mod 10 maka akan dibuat sebuah file (.txt) dengan format {jumlah-gacha}[tipe-item]{rarity}{name}{sisa-primogems}
```c
else if(gacha%10==0)
{
    int x = 0;
    for(int i = gacha; i < gacha + 90; i++)
    {
        if(primogems < 160) break;
        printf("primo: %d\n", primogems);
        time_t current_time;
        struct tm * time_info;
        char filename[100];
        time(&current_time);
        time_info = localtime(&current_time);
        strftime(filename, sizeof(filename), "%H:%M:%S", time_info);
        strcat(filename, "_gacha_");
        char numGacha[100];
        sprintf(numGacha, "%d", i-x-10);
        strcat(filename, numGacha); strcat(filename, ".txt");
```

Kemudian dilakukan looping sebanyak 10 kali, jika jumlah gacha genap maka dilakukan print gacha weapons, dan jika jumlah gachanya ganjil dilakukan print gacha character. hasilnya akan berada di file (.txt) yang dibuat sebelumnya. Karena file database berformat (.json) maka program harus membaca dan parsing file (.json) dengan cara sebagai berikut
```c
for(int j=0; j<10;j++)
{
    if (primogems >= 160 && !done)
    {
        primogems-=160;
        char buf1[10000], counter[100];
        sprintf(counter, "%d", i-x-9);
        char pathFolder[100] = "gacha_gacha", outputFile[200] = "";
        strcat(outputFile, counter);
        strcat(outputFile, "_");
        char tipe[15];
        int mode; // mode 1: weapons, mode 2: character
        if((i-x-9)%2==0) mode = 1;
        else mode = 2;
        if(mode == 1) strcpy(tipe, "weapons_");
        else strcpy(tipe, "characters_");
        strcat(outputFile, tipe);
        FILE *fp;
        char buffer[5000];
        struct json_object *parsed_json;
        struct json_object *nameJson;
        struct json_object *rarityJson;
        char jsonPath[300]="/home/gaudhiwaa/";
        int gachaVal;
        if(mode == 1) strcat(jsonPath, "weapons"), gachaVal = rand()%130;
        else strcat(jsonPath, "characters"), gachaVal = rand()%48;
        char jsonFileName[100];
        strcpy(jsonFileName, gachaFileName(jsonPath,gachaVal, 0));
        strcat(jsonPath, "/");
        strcat(jsonPath, jsonFileName);
        fp = fopen(jsonPath,"r");
        fread(buffer, 5000, 1, fp);
        fclose(fp);
        parsed_json = json_tokener_parse(buffer);
        json_object_object_get_ex(parsed_json, "name", &nameJson);
        json_object_object_get_ex(parsed_json, "rarity", &rarityJson);
        char rarity[2];
        strcpy(rarity, json_object_get_string(rarityJson));
        printf("rarity: %s\n", rarity);
        char name[100];
        strcpy(name, json_object_get_string(nameJson));
        printf("name: %s\n", name);
        strcat(outputFile, rarity); strcat(outputFile, "_");
        strcat(outputFile, name); strcat(outputFile, "_");
        char primoLeft[100];
        sprintf(primoLeft, "%d", primogems);
        strcat(outputFile, primoLeft);
        printf("outputFile: %s\n", outputFile);
        char txtPath[100];
        strcpy(txtPath, currFolder);
        strcat(txtPath, "/");
        strcat(txtPath, filename);
        printf("txtpath: %s\n", txtPath);
        snprintf(buf1, sizeof buf1, "%s", txtPath);
        FILE *data;
        data = fopen(buf1, "a+");
        fprintf(data, "%s\n", outputFile);
        fclose(data);
        i++;
        sleep(0.1);
    }
    else done = 1;
}
```

Dalam program diatas, gacha dilakukan dengan merandom angka sesuai jumlah file, contoh untuk weapons dilakukan random sebanyak 130, untuk character dilakukan random sebanyak 48. Kemudian hasil random tadi (gachaVal) dicocokkan dengan file json dengan index yang sama dengan gachaVal.
```c
char* gachaFileName(char *base, int gachaNum, int i)
{
    char path[1000];
    struct dirent *dt;
    DIR *dir = opendir(base);
    char *result;
    int found = 0;
    while ((dt = readdir(dir)) != NULL && found == 0)
    {
        if (strcmp(dt->d_name, ".") != 0 && strcmp(dt->d_name, "..") != 0)
        {
            result = dt->d_name;
            if (i == gachaNum) found = 1;
            i++;
        }
    }
    closedir(dir);
    return result;
}
```

Jika primogemsnya habis atau kurang dari 160, maka gacha selesai dilakukan
```c
if(primogems < 160) printf("Gachanya selesai, primogems habis.");
```

Kemudian 3 jam setelah jam 4.44 yaitu jam 7.44, program akan mengzip folder gacha_gacha menjadi not_safe_for_wibu.zip dengan password "satuduatiga"
```c
else if(timeinfo.tm_mday == 30 && timeinfo.tm_mon + 1 == 3 && timeinfo.tm_hour == 7 && timeinfo.tm_min == 44)
        //else if(timeinfo.tm_mday == 19 && timeinfo.tm_mon + 1 == 3 && timeinfo.tm_hour == 19 && timeinfo.tm_min == 15)
        {
            // Jam 7:44 abis anniv
            if(!encrypted)
            {
                encrypted = 1;
                while(wait(NULL) > 0);
                pid_t pid = fork();
                if (pid < 0) exit(EXIT_FAILURE);
                if (pid == 0)
                {
                    char *arg10[] = {"zip", "-r", "not_safe_for_wibu.zip", "gacha_gacha", "--password", "satuduatiga", NULL};
                    execv("/bin/zip", arg10);
                }


kemudian program akan menghapus folder gacha_gacha, characters, dan weapons seperti berikut
```c
while(wait(NULL) > 0);
pid = fork();
if (pid < 0) exit(EXIT_FAILURE);
if (pid == 0)
{
    char *arg11[] = {"rm", "-r", "gacha_gacha", NULL};
    execv("/bin/rm", arg11);
}

while(wait(NULL) > 0);
pid = fork();
if (pid < 0) exit(EXIT_FAILURE);
if (pid == 0)
{
    char *arg11[] = {"rm", "-r", "characters", NULL};
    execv("/bin/rm", arg11);
}

while(wait(NULL) > 0);
pid = fork();
if (pid < 0) exit(EXIT_FAILURE);
if (pid == 0)
{
    char *arg11[] = {"rm", "-r", "weapons", NULL};
    execv("/bin/rm", arg11);
}
```

![hasil](./screenshot/1a.png)
![hasil](./screenshot/1b.png)
![hasil](./screenshot/1c.png)
![hasil](./screenshot/1d.png)
![hasil](./screenshot/1e.png)


2. Japrun bekerja di sebuah perusahaan dibidang review industri perfilman, karena kondisi saat ini sedang pandemi Covid-19, dia mendapatkan sebuah proyek untuk mencari drama korea yang tayang dan sedang ramai di Layanan Streaming Film untuk diberi review. Japrun sudah mendapatkan beberapa foto-foto poster serial dalam bentuk zip untuk diberikan review, tetapi didalam zip tersebut banyak sekali poster drama korea dan dia harus memisahkan poster-poster drama korea tersebut tergantung dengan kategorinya. Japrun merasa kesulitan untuk melakukan pekerjaannya secara manual, kamu sebagai programmer diminta Japrun untuk menyelesaikan pekerjaannya.

    **a**. Hal pertama yang perlu dilakukan oleh program adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift2/drakor”. Karena atasan Japrun teledor, dalam zip tersebut bisa berisi folder-folder yang tidak penting, maka program harus bisa membedakan file dan folder sehingga dapat memproses file yang seharusnya dikerjakan dan menghapus folder-folder yang tidak dibutuhkan.

    **b**. Poster drama korea perlu dikategorikan sesuai jenisnya, maka program harus membuat folder untuk setiap jenis drama korea yang ada dalam zip. Karena kamu tidak mungkin memeriksa satu-persatu manual, maka program harus membuatkan folder-folder yang dibutuhkan sesuai dengan isi zip.
**Contoh: Jenis drama korea romance akan disimpan dalam `“/drakor/romance”`, jenis drama korea action akan disimpan dalam `“/drakor/action”` , dan seterusnya.**

    **c**. Setelah folder kategori berhasil dibuat, program akan memindahkan poster ke folder dengan kategori yang sesuai dan di rename dengan nama.
**Contoh:**` “/drakor/romance/start-up.png”`.

    **d**. Karena dalam satu foto bisa terdapat lebih dari satu poster maka foto harus di pindah ke masing-masing kategori yang sesuai. Contoh: foto dengan nama `“start-up;2020;romance_the-k2;2016;action.png”` dipindah ke folder `“/drakor/romance/start-up.png”` dan `“/drakor/action/the-k2.png”`.

    **e**. Di setiap folder kategori drama korea buatlah sebuah file "data.txt" yang berisi nama dan tahun rilis semua drama korea dalam folder tersebut, jangan lupa untuk sorting list serial di file ini berdasarkan tahun rilis (Ascending). `Format harus sesuai contoh dibawah ini`.

    ### Penyelesaian Soal 2
    - `Nomor 2 a` pada soal a, file.zip berada pada lokasi yang dapat ditentukan sendiri oleh user, extract file.zip dapat dilakukan menggunakan key unzip dan lokasinya berada pada folder `~/shift2/drakor` yang mana digenerate melalui program dengan menggunakan key mkdir dengan menyertakan lokasi (nama folder). Kemudian, untuk menyortir hanya file dengan ekstensi .png dalam folder, dapat menambahkan option `"-x, "/" " `pada key unzip. implementasinya dapat dilihat pada code fungsi unzip berikut.
    ```c
    void unzip(){
    pid_t pid = fork();
    char dest[] = "/home/labib/shift2/drakor";
        if(pid < 0){
            exit(EXIT_FAILURE);
        }
        if(pid == 0){
            char *argc[] = {"mkdir", "-p", dest, NULL};
            execv("/bin/mkdir", argc);
        }
        while(wait(NULL) > 0);
        pid = fork();
        if (pid < 0) {
            exit(EXIT_FAILURE);
        }
        if (pid == 0) {
            chdir("/home/labib/shift2/drakor");
            char *argc2[] = {"unzip", "../../drakor.zip", "-x", "*/*", NULL};
            execv("/usr/bin/unzip", argc2);
        }
    }
    ```
    dokumentasi :
    ![hasil](./screenshot/2a.png)
    - `Nomor 2 b` pada soal b, yang pertama dilakukan adalah melakukan list nama file image dan menyimpan kategori - kategori yang ada. Untuk melakukan list dapat menggunakan fungsi list directory dari modul, lalu kita pisahkan judul, tahun rilis, dan kategori menggunakan fungsi strtok() dengan pembanding character "_" untuk memisahkan file yang terdapat dua judul film dan dengan pembanding character ";" (implementasi lihat di code, simpan hasil pemisahan pada variabel seperti di code). setelahnya. Selanjutnya, kita pastikan bahwa setelah pemisahan hanya mengandung string kategori karena ada kemungkinan tambahan character ".". Caranya dengan mengecek kategori yang masih mengandung "." lalu erase ".". Terakhir, hasil beberapa operasi tadi disimpan pada variabel `makedir`. Langkah terakhir, kita buat folder dengan menggunakan key mkdir dengan lokasi tujuan `~/shift2/drakor`.
    ```c
    {
        pid = fork();
        
        if(pid < 0)exit(EXIT_FAILURE);
        if(pid == 0){
            list();
            for(int i = 0; i<files; i++)
            {   
                pid = fork();
                if(pid < 0)exit(EXIT_FAILURE);
                if(pid == 0){
                    // printf("%d %s\n",i, title[i]);
                    char *argc[] = {"mkdir", "-p",makedir[i], NULL};
                    execv("/bin/mkdir", argc);
                }
            }
            while((wait(NULL)) > 0);
        }
    }
    void list()
    {   char path[1000];   
        DIR *folder;
        struct dirent *entry;
        folder = opendir("/home/labib/shift2/drakor");
        char title[100][100];
        char year[100][100];
        char cat[100][100];
        char save[100][100];
        if(folder == NULL)
        {
            perror("Unable to read directorys");
            return;
        }

        while( (entry=readdir(folder)))
        {   
            
            if(strcmp(entry->d_name, ".")!=0 && strcmp(entry->d_name, "..") !=0){
                strtok(entry->d_name, "_");
                strcpy(title[files], strtok(entry->d_name, ";"));
                strcpy(year[files], strtok(NULL, ";"));
                strcpy(cat[files], strtok(NULL, "p"));
                if(strcmp(&cat[files][strlen(cat[files])-1], ".")==0){
                    cat[files][strlen(cat[files])-1] = '\0';
                }

            }
            sprintf(makedir[files], "%s%s", "/home/labib/shift2/drakor/", cat[files]);
            files++;

        }

        closedir(folder);
    }
    ```
    dokumentasi :
    ![hasil](./screenshot/2b.png)
    - `Nomor 2 c dan 2 d` pada soal c, pemindahan file poster ke folder sesuai dengan kategorinya dapat dilakukan dengan menggunakan key mv, tetapi sebelumnya harusnya melakukan beberapa operasi untuk memenuhi kondisi soal, sebagai tambahan bahwa soal 2 d akan digabungkan dengan 2 c. Langkah pertama adalah, melakukan list file apa saja yang terdapat pada folder `drakor` dan akan kita lakukan duplikasi apabila dalam satu file poster terdapat dua film dengan cara mengeceknya dengan fungsi strtok pemisah "_" hasil yang diperoleh akan disimpan dalam variabel listfFile (implementasi lihat pada code). Selanjutnya, kita akan memisahkan judul, tahun rilis, dan kategori dengan cara yang sama seperti pada soal b. Untuk file dalam listfFile yang tidak mengandung `.png` akan kita duplikasi (yang tidak mengandung .png artinya file yang terdiri dari dua poster). Terakhir, barulah kita pindahkan semua file sesuai pada kategori  berdasarkan index variabel fCat dan fTitle.
    ```c
    {
        pid = fork();
        if(pid < 0)exit(EXIT_FAILURE);
        if(pid == 0){
            file();
            char temp[200];
            char source[2000], dest__[2000];
            
            for (int i = 0; i < fileList; i++)
            {   
                sprintf(source,"%s/%s",loc,lisfFile[i]);
                strcpy(temp, lisfFile[i]);
                strcpy(fTitle[i], strtok(temp, ";"));
                strcpy(fYear[i], strtok(NULL, ";"));
                strcpy(fCat[i], strtok(NULL, "p"));
                
                if(strcmp(&fCat[i][strlen(fCat[i])-1], ".")==0){
                    fCat[i][strlen(fCat[i])-1] = '\0'; 
                }
                //printf("%s", temp);
                //printf("%s %s %s\n", fTitle[i], fYear[i], fCat[i]);
                
                if(strstr(lisfFile[i], ".png") == NULL){
                    while((wait(NULL))>0);

                    pid = fork();
                    sprintf(source, "%s/%s%s%s", loc, lisfFile[i], under, lisfFile[i+1]);
                    sprintf(dest__, "%s/%s.png", loc, lisfFile[i]);

                    if(pid < 0) exit(EXIT_FAILURE);
                    if(pid == 0){
                        char *argc[] = {"cp", source, dest__, NULL};
                        execv("/bin/cp", argc);
                    }

                    while((wait(NULL))>0);

                    pid = fork();
                    sprintf(source, "%s/%s%s%s", loc, lisfFile[i], under, lisfFile[i+1]);
                    sprintf(dest__, "%s/%s", loc, lisfFile[i+1]);

                    if(pid < 0)exit(EXIT_FAILURE);
                    if(pid == 0){
                        char *argc1[] = {"mv", source, dest__, NULL};
                        execv("/bin/mv", argc1);
                    }

                    strcat(lisfFile[i],".png");

                }
                while((wait(NULL))>0);
                sprintf(source, "%s/%s", loc, lisfFile[i]);
                sprintf(dest__, "%s/%s/%s.png", loc, fCat[i], fTitle[i]);
                //printf("%d %s \n",i, lisfFile[i]);
                //printf("%s %s %s\n", fTitle[i], fYear[i], fCat[i]);
                pid = fork();
                if(pid < 0)exit(EXIT_FAILURE);
                if(pid == 0){
                    char *argc2[] = {"mv", source, dest__, NULL};
                    execv("/bin/mv", argc2);
                }
            }
        //soal 2 e
        }
    }
    void file(){
        DIR *folder;
        struct dirent *entry;
        char *token;
        folder = opendir("/home/labib/shift2/drakor");
        if(folder == NULL)
        {
            perror("Unable to read directory");
            exit(EXIT_FAILURE);
        }

        while( (entry=readdir(folder)) )
        {   
            if(strstr(entry->d_name, ".") && strcmp(entry->d_name, ".")!=0 && strcmp(entry->d_name, "..") !=0){
                token = strtok(entry->d_name, "_");

                while( token != NULL){
                    strcpy(listfFile[fileList++], token);
                    token = strtok(NULL, "_");
                }
            }
        }
        closedir(folder);
    }
    ```
    dokumentasi :
    ![hasil](./screenshot/2cd.png)
    - `Nomor 2 e` pada soal e, pembuatan file.txt di setiap folder kategori dapat menggunakan key touch. Lalu, kita buat i/o file untuk menulis pada file.txt. Namun, sama seperti sebelumnya kita list nama folder tersebut lalu dengan memanfaatkan tiga variabel (judul, tahun liris, kategori) yang sebelumnya telah kita peroleh pada soal c/d kita bisa dengan langsung menuliskan judul dan tahun rilis dengan membandingkan kategori pada index judul film bersangkutan dengan listFolder, apabila sama tulis pada file.txt. Tambahan pada soal, judul dan tahun rilis harus disort berdasarkan tahun rilis secara Ascending. Pada penyelesaiannya digunakan metode quick sort karena jumlah file sementara sedikit dan implementasinya cukup sederhanan.
    ```c
    {
        FILE *fptr;
        folder(loc);
        //folder();
        for(int i = 0;i < folderList;i++){
            printf("%s\n", listFolder[i]);
        }
        char dest[2000];
        for (int i = 0; i < folderList; i++)
        {   while((wait(NULL)) > 0);
            sprintf(dest, "%s/%s/data.txt", loc, listFolder[i]);
            char *argc[] = {"touch", dest, NULL};
            pid = fork();
            if(pid < 0) exit(EXIT_FAILURE);
            if(pid == 0){
                execv("/bin/touch", argc);
            }
            while((wait(NULL)) > 0);
            pid = fork();
            if(pid < 0) exit(EXIT_FAILURE);
            if(pid == 0){     
                sprintf(dest, "%s/%s/data.txt", loc, listFolder[i]);
                fptr = fopen(dest, "w+");
                fprintf(fptr, "kategori : %s\n\n",listFolder[i]);

                char str[500][500];
                int tp = 0;
                for(int j = 0; j < fileList; j++){
                    if(strcmp(listFolder[i], fCat[j])==0){
                        sprintf(str[tp], "nama : %s\nrilis : tahun %s\n\n", fTitle[j], fYear[j]);
                        tp++;
                    }
                }
                //printf("Ini bag %d\n", i);
                int n = sizeof(str)/sizeof(str[0]);
                printf("%d\n",tp);
                int min_id;
                for(int a = 0; a < tp-1; a++){
                    min_id = a;
                    for(int b = a+1; b<tp;b++){
                        if(atoi(str[b]+strlen(str[b])-4) < atoi(str[min_id]+strlen(str[min_id])-4)){
                            min_id = b;
                        }
                    }
                    char temp[100];
                    strcpy(temp, str[min_id]);
                    strcpy(str[min_id], str[a]);
                    strcpy(str[a], temp);
                }
                for (int c = 0; c < tp; c++){
                    fprintf(fptr, "%s", str[c]);
                }
                fclose(fptr);
                exit(0);

            }
        }
    }
    void folder (char *basePath) {
        char path[1000];
        struct dirent *dp;
        DIR *dir = opendir(basePath);
        char *token;

        if (!dir) return;

        while ((dp = readdir(dir)) != NULL) {
            if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) {
                strcpy(listFolder[folderList++],dp->d_name);
            }
        }
        closedir(dir);
    }
    ```
    dokumentasi :
    ![hasil](./screenshot/2e.png)
3. Conan adalah seorang detektif terkenal. Suatu hari, Conan menerima beberapa laporan tentang hewan di kebun binatang yang tiba-tiba hilang. Karena jenis-jenis hewan yang hilang banyak, maka perlu melakukan klasifikasi hewan apa saja yang hilang

    **a**. Untuk mempercepat klasifikasi, Conan diminta membuat program untuk membuat 2 directory di “/home/[USER]/modul2/” dengan nama “darat” lalu 3 detik kemudian membuat directory ke 2 dengan nama “air”. 

    **b**. Kemudian program diminta dapat melakukan extract “animal.zip” di “/home/[USER]/modul2/”.

    **c**. Tidak hanya itu, hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama filenya. Untuk hewan darat dimasukkan ke folder “/home/[USER]/modul2/darat” dan untuk hewan air dimasukkan ke folder “/home/[USER]/modul2/air”. Rentang pembuatan antara folder darat dengan folder air adalah 3 detik dimana folder darat dibuat terlebih dahulu. Untuk hewan yang tidak ada keterangan air atau darat harus dihapus.

    **d**. Setelah berhasil memisahkan hewan berdasarkan hewan darat atau hewan air. Dikarenakan jumlah burung yang ada di kebun binatang terlalu banyak, maka pihak kebun binatang harus merelakannya sehingga conan harus menghapus semua burung yang ada di directory “/home/[USER]/modul2/darat”. Hewan burung ditandai dengan adanya “bird” pada nama file.

    **e**. Terakhir, Conan harus membuat file list.txt di folder “/home/[USER]/modul2/air” dan membuat list nama semua hewan yang ada di directory “/home/[USER]/modul2/air” ke “list.txt” dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut.
Contoh : conan_rwx_hewan.png

### Penyelesaian Soal 3
- `Nomor 3 a dan 3 b` Untuk soal ini kita akan membuat direktori /home/gaudhiwaa/modul2, /home/gaudhiwaa/modul2/darat, dan /home/gaudhiwaa/modul2/air. Kita inisialisasi pid_t child_id terlebih dahulu, kemudian kita fork(). Jika proses child berjalan (child_id==0), maka kita membuat file modul2 dengan char *argv[] = {"mkdir", "-p", "/home/gaudhiwaa/modul2", NULL}; execv("/bin/mkdir", argv);. Lalu lakukan child_id2 = fork(), setelah itu kita ekstrak animal.zip dengan cara char *arg[] = {"unzip","-qq","/home/gaudhiwaa/Downloads/animal.zip","-d","./", NULL}; execv("/usr/bin/unzip",arg);. Lalu lakukan fork(), setelah itu kita buat direktori darat dan air, dengan cara char *argv[] = {"mkdir", "-p", "/home/gaudhiwaa/modul2/darat", NULL}; execv("/bin/mkdir", argv); dan char *argv[] = {"mkdir", "-p", "/home/gaudhiwaa/modul2/air", NULL}; execv("/bin/mkdir", argv);. Antara pembuatan tersebut, kita menggunakan sleep(3), sehingga file darat terlebih dahulu dibuat dan file air 3 detik setelahnya.
```c
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>

int main() {
  pid_t child_id, child_id2, child_id3, child_id4, child_id5, child_id6, child_id7, child_id8, child_id9;
  int status;

  child_id = fork();
  FILE * fp;

  char *list_bird[]={"find","/home/gaudhiwaa/modul2/air","-iname","*air*.jpg", NULL};  
  
  if (child_id < 0) {
    exit(EXIT_FAILURE); 
  }
      if (child_id == 0) {
        
        char *argv[] = {"mkdir", "-p", "/home/gaudhiwaa/modul2", NULL};
        execv("/bin/mkdir", argv);

      } else {

        while ((wait(&status)) > 0);
        child_id2 = fork();

        if (child_id2 == 0) {

          char *arg[] = {"unzip","-qq","/home/gaudhiwaa/Downloads/animal.zip","-d","./", NULL};
          execv("/usr/bin/unzip",arg);
          
        } else {

          while ((wait(&status)) > 0);
          child_id3 = fork();
          
          if (child_id3 == 0) {

            char *argv[] = {"mkdir", "-p", "/home/gaudhiwaa/modul2/darat", NULL};
            execv("/bin/mkdir", argv);
          
          } else {

            while ((wait(&status)) > 0);
            sleep(3);
            child_id4 = fork();
            
            if(child_id4 == 0) {

              char *argv[] = {"mkdir", "-p", "/home/gaudhiwaa/modul2/air", NULL};
              execv("/bin/mkdir", argv);
            } 
```

![hasil](./screenshot/3ab.png)

- `Nomor 3c dan 3d` Selanjutnya untuk memisahkan hewan darat dan air dan memasukannya ke dalam masing-masing direktori, maka kita gunakan char *darat[]={"find","/home/gaudhiwaa/Downloads/animal","-iname","*darat*.jpg","-exec","cp","{}","/home/gaudhiwaa/modul2/darat",";",NULL}; execv("/usr/bin/find",darat);. Dengan pola yang sama kita lakukan hal tersebut juga pada direktori air. Setelah itu kita hapus file ekstrak yang telah dibuat dengan char *rm_notDaratAir[]={"rm", "-r" ,"/home/gaudhiwaa/Downloads/animal", NULL}; execv("/bin/rm", rm_notDaratAir);. Selanjutnya untuk remove file bird, kita gunakan char *rm_bird[]={"find","/home/gaudhiwaa/modul2/darat","-iname","*bird*.jpg","-exec","rm", "-rf","{}",";", NULL}; execv("/usr/bin/find", rm_bird);.

```c
                while ((wait(&status)) > 0);
                child_id5 = fork();

                if(child_id5 == 0) {

                  char *darat[]={"find","/home/gaudhiwaa/Downloads/animal","-iname","*darat*.jpg","-exec","cp","{}","/home/gaudhiwaa/modul2/darat",";",NULL};  
                  execv("/usr/bin/find",darat);
                } else {

                    while ((wait(&status)) > 0);
                    child_id6 = fork();

                    if(child_id6 == 0) {

                        char *air[]={"find","/home/gaudhiwaa/Downloads/animal","-iname","*air*.jpg","-exec","cp","{}","/home/gaudhiwaa/modul2/air",";",NULL};  
                        execv("/usr/bin/find",air);
                    } else {
                        while ((wait(&status)) > 0);
                        child_id7 = fork();

                        if(child_id7 == 0) {

                          char *rm_notDaratAir[]={"rm", "-r" ,"/home/gaudhiwaa/Downloads/animal", NULL};  
                          execv("/bin/rm", rm_notDaratAir);
                        } else {
                          while ((wait(&status)) > 0);
                          child_id8 = fork();

                          if(child_id8 == 0) {

                            char *rm_bird[]={"find","/home/gaudhiwaa/modul2/darat","-iname","*bird*.jpg","-exec","rm", "-rf","{}",";", NULL};  
                            execv("/usr/bin/find", rm_bird);
                          } else {
```

![hasil](./screenshot/3cd-1.png)
![hasil](./screenshot/3cd-2.png)

- `Nomor 3e` Kemudian untuk mendapatkan UID_[UID file permission]_Nama File.[jpg/png] dijalankan fungsi listFile().
```c
        void listFile(){
    FILE *fptr;
    struct dirent *dt;
    char path[] = "/home/gaudhiwaa/modul2/air/";
    DIR *dir = opendir(path);

    fptr = fopen("/home/gaudhiwaa/modul2/air/list.txt", "w");
    if(fptr == NULL){
      printf("ERROR!\n");
      exit(1);
    }

    while ((dt = readdir(dir)) != NULL){
      struct stat fs;
      int r = stat(path, &fs);
      if(r == -1){
        fprintf(stderr, "File Error\n");
        exit(1);
      }
      register struct passwd *pw;
      uid_t uid = getuid();
      pw = getpwuid(uid);
      if(strcmp(dt->d_name,".") != 0 && strcmp(dt->d_name, "..") != 0 && strcmp(dt->d_name, "list.txt") != 0){
        char userp[256] = "";
        if(fs.st_mode & S_IRUSR)
          strcat(userp, "r");
        if(fs.st_mode & S_IWUSR)
          strcat(userp, "w");
        if(fs.st_mode & S_IXUSR)
          strcat(userp, "x");
        fprintf(fptr,"%s_%s_%s\n",pw->pw_name,userp, dt->d_name);
      }
    }
    fclose(fptr);
}
```

![hasil](./screenshot/3e-1.png)
