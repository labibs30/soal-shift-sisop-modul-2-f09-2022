#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <wait.h>
#include <sys/stat.h>
void list();
void unzip();
void file();
void folder (char *basePath);
char makedir[500][500];
char listFolder[100][100];
char listfFile[500][500];
char fTitle[500][500];
char fYear[500][500];
char fCat[500][500];
int files = 0;
int fileList = 0;
int folderList = 0;
char loc[100] = "/home/labib/shift2/drakor";
char sort[100][500];

int main(int argc, char const *argv[])
{   
    pid_t pid;
    if(pid < 0){
        exit(EXIT_FAILURE);
    }
    if(pid == 0){
        unzip();
    }
    while((wait(NULL))>0);
    //Soal 2.b
    pid = fork();
    
    if(pid < 0)exit(EXIT_FAILURE);
    if(pid == 0){
        list();
        for(int i = 0; i<files; i++)
        {   
            pid = fork();
            if(pid < 0)exit(EXIT_FAILURE);
            if(pid == 0){
                // printf("%d %s\n",i, title[i]);
                char *argc[] = {"mkdir", "-p",makedir[i], NULL};
                execv("/bin/mkdir", argc);
            }
        }
        while((wait(NULL)) > 0);
    }
    while((wait(NULL)) > 0);
    //Soal 2.c dan 2.d
    pid = fork();
    if(pid < 0)exit(EXIT_FAILURE);
    if(pid == 0){
        file();
        char temp[200];
        char source[2000], dest__[2000];
        char under[]="_";
        char png[] = ".png";
        for (int i = 0; i < fileList; i++)
        {   
            sprintf(source,"%s/%s",loc,listfFile[i]);
            strcpy(temp, listfFile[i]);
            strcpy(fTitle[i], strtok(temp, ";"));
            strcpy(fYear[i], strtok(NULL, ";"));
            strcpy(fCat[i], strtok(NULL, "p"));
            
            if(strcmp(&fCat[i][strlen(fCat[i])-1], ".")==0){
                fCat[i][strlen(fCat[i])-1] = '\0'; 
            }
            //printf("%s", temp);
            //printf("%s %s %s\n", fTitle[i], fYear[i], fCat[i]);
            if(strstr(listfFile[i], ".png") == NULL){
                while((wait(NULL))>0);

                pid = fork();
                sprintf(source, "%s/%s%s%s", loc, listfFile[i], under, listfFile[i+1]);
                sprintf(dest__, "%s/%s.png", loc, listfFile[i]);

                if(pid < 0) exit(EXIT_FAILURE);
                if(pid == 0){
                    char *argc[] = {"cp", source, dest__, NULL};
                    execv("/bin/cp", argc);
                }

                while((wait(NULL))>0);

                pid = fork();
                sprintf(source, "%s/%s%s%s", loc, listfFile[i], under, listfFile[i+1]);
                sprintf(dest__, "%s/%s", loc, listfFile[i+1]);

                if(pid < 0)exit(EXIT_FAILURE);
                if(pid == 0){
                    char *argc1[] = {"mv", source, dest__, NULL};
                    execv("/bin/mv", argc1);
                }
                strcat(listfFile[i],".png");
            }
            while((wait(NULL))>0);
            sprintf(source, "%s/%s", loc, listfFile[i]);
            sprintf(dest__, "%s/%s/%s.png", loc, fCat[i], fTitle[i]);
            //printf("%d %s \n",i, listfFile[i]);
            //printf("%s %s %s\n", fTitle[i], fYear[i], fCat[i]);
            pid = fork();
            if(pid < 0)exit(EXIT_FAILURE);
            if(pid == 0){
                char *argc2[] = {"mv", source, dest__, NULL};
                execv("/bin/mv", argc2);
            }
        }
        while((wait(NULL)) > 0);
        //Soal 2.e
        FILE *fptr;
        folder(loc);
        //folder();
        for(int i = 0;i < folderList;i++){
            printf("%s\n", listFolder[i]);
        }
        char dest[2000];
        for (int i = 0; i < folderList; i++)
        {   while((wait(NULL)) > 0);
            sprintf(dest, "%s/%s/data.txt", loc, listFolder[i]);
            char *argc[] = {"touch", dest, NULL};
            pid = fork();
            if(pid < 0) exit(EXIT_FAILURE);
            if(pid == 0){
                execv("/bin/touch", argc);
            }
            while((wait(NULL)) > 0);
            pid = fork();
            if(pid < 0) exit(EXIT_FAILURE);
            if(pid == 0){     
                sprintf(dest, "%s/%s/data.txt", loc, listFolder[i]);
                fptr = fopen(dest, "w+");
                fprintf(fptr, "kategori : %s\n\n",listFolder[i]);

                char str[500][500];
                int tp = 0;
                for(int j = 0; j < fileList; j++){
                    if(strcmp(listFolder[i], fCat[j])==0){
                        sprintf(str[tp], "nama : %s\nrilis : tahun %s\n\n", fTitle[j], fYear[j]);
                        tp++;
                    }
                }
                //printf("Ini bag %d\n", i);
                int n = sizeof(str)/sizeof(str[0]);
                printf("%d\n",tp);
                int min_id;
                for(int a = 0; a < tp-1; a++){
                    min_id = a;
                    for(int b = a+1; b<tp;b++){
                        if(atoi(str[b]+strlen(str[b])-4) < atoi(str[min_id]+strlen(str[min_id])-4)){
                            min_id = b;
                        }
                    }
                    char temp[100];
                    strcpy(temp, str[min_id]);
                    strcpy(str[min_id], str[a]);
                    strcpy(str[a], temp);
                }
                for (int c = 0; c < tp; c++){
                    fprintf(fptr, "%s", str[c]);
                }
                fclose(fptr);
                exit(0);

            }
        }
    }
}
void unzip(){
    pid_t pid = fork();
    char dest[] = "/home/labib/shift2/drakor";
    if(pid < 0){
        exit(EXIT_FAILURE);
    }
    if(pid == 0){
        char *argc[] = {"mkdir", "-p", dest, NULL};
        execv("/bin/mkdir", argc);
    }
    while(wait(NULL) > 0);
    pid = fork();
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }
    if (pid == 0) {
        chdir("/home/labib/shift2/drakor");
        char *argc2[] = {"unzip", "../../drakor.zip", "-x", "*/*", NULL};
        execv("/usr/bin/unzip", argc2);
    }
}

void list()
{   char path[1000];   
    DIR *folder;
    struct dirent *entry;
    folder = opendir("/home/labib/shift2/drakor");
    char title[100][100];
    char year[100][100];
    char cat[100][100];
    char save[100][100];
    if(folder == NULL)
    {
        perror("Unable to read directorys");
        return;
    }

    while( (entry=readdir(folder)))
    {   
        
        if(strcmp(entry->d_name, ".")!=0 && strcmp(entry->d_name, "..") !=0){
            strtok(entry->d_name, "_");
            strcpy(title[files], strtok(entry->d_name, ";"));
            strcpy(year[files], strtok(NULL, ";"));
            strcpy(cat[files], strtok(NULL, "p"));
            if(strcmp(&cat[files][strlen(cat[files])-1], ".")==0){
                cat[files][strlen(cat[files])-1] = '\0';
            }

        }
        sprintf(makedir[files], "%s%s", "/home/labib/shift2/drakor/", cat[files]);
        files++;

    }
    closedir(folder);
}

void file(){
    DIR *folder;
    struct dirent *entry;
    char *token;
    folder = opendir("/home/labib/shift2/drakor");
    if(folder == NULL)
    {
        perror("Unable to read directory");
        exit(EXIT_FAILURE);
    }

    while( (entry=readdir(folder)) )
    {   
        if(strstr(entry->d_name, ".") && strcmp(entry->d_name, ".")!=0 && strcmp(entry->d_name, "..") !=0){
            token = strtok(entry->d_name, "_");

            while( token != NULL){
                strcpy(listfFile[fileList++], token);
                token = strtok(NULL, "_");
            }
        }
    }
    closedir(folder);
}

void folder (char *basePath) {
    char path[1000];
    struct dirent *dp;
    DIR *dir = opendir(basePath);
    char *token;

    if (!dir) return;

    while ((dp = readdir(dir)) != NULL) {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) {
            strcpy(listFolder[folderList++],dp->d_name);
        }
    }
    closedir(dir);
}